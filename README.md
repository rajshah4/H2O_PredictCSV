Trying to get a H2O model into production using PredictCSV - this is just a toy model with 3 inputs, 1 output (regression), using a RF with 2 Trees.  

I have created the POJO and grabed the PredictCsv.java file from the version 3 H2O repo.  
I changed line 134 of PredictCsv.java to .toString() instead of .toHexString()  

I then compile this:
```
javac -cp h2o-genmodel.jar -J-Xmx2g PredictCsv.java DRF_model_R_1470416938086_15.java
```

Next I feed it my one line input file:
```
java -ea -cp .:h2o-genmodel.jar hex.genmodel.tools.PredictCsv --header --model DRF_model_R_1470416938086_15 --input input.csv --output output.csv
```

The problem is the output is in hex, e.g,:
```
0x1.a24dd2p-2
```